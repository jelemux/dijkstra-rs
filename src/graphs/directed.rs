use super::Edge;
use num_traits::Num;
use std::collections::HashMap;
use std::fmt::Debug;
use std::fmt::Display;
use std::hash::Hash;
use std::ops::Add;
use std::ops::Div;
use std::ops::Mul;
use std::ops::Rem;
use std::ops::Sub;

/// Data structure that represents "connections" between pairs of elements.
///
/// Elements are called nodes.
/// The connections are called edges.
/// The connections between nodes go in one or both directions.
#[derive(Clone, Debug)]
pub struct DirectedGraph<NODE, WEIGHT>
where
    NODE: Debug + Display + Copy + Eq + Hash,
    WEIGHT: Num + Debug + Copy + PartialOrd + Hash,
{
    edges: HashMap<NODE, HashMap<NODE, WEIGHT>>,
}

impl<NODE, WEIGHT> DirectedGraph<NODE, WEIGHT>
where
    NODE: Debug + Display + Copy + Eq + Hash,
    WEIGHT: Num + Debug + Copy + PartialOrd + Hash,
{
    /// Creates an empty graph.
    pub fn new() -> Self {
        Self {
            edges: HashMap::new(),
        }
    }

    /// Returns a reference to the `edges` of the graph.
    pub fn edges(&self) -> &HashMap<NODE, HashMap<NODE, WEIGHT>> {
        &self.edges
    }

    /// Returns all nodes in the graph.
    pub fn nodes(&self) -> Vec<NODE> {
        self.edges.keys().cloned().collect()
    }

    /// Returns the weight of the edge between the nodes if the edge exists.
    pub fn weight(&self, src: &NODE, dest: &NODE) -> Option<WEIGHT> {
        match self.edges.get(src) {
            None => None,
            Some(edge) => match edge.get(dest) {
                None => None,
                Some(weight) => Some(*weight),
            },
        }
    }

    /// Inserts an edge into the graph.
    ///
    /// If the graph did not have the edge present, [`None`] is returned.
    ///
    /// If the graph did have this edge present, the value is updated, and the old
    /// value is returned.
    pub fn insert_edge(&mut self, edge: Edge<NODE, WEIGHT>) -> Option<Edge<NODE, WEIGHT>> {
        let old_edge;
        match self.edges.get_mut(&edge.src) {
            None => {
                self.replace_all_edges_from_src(&edge);
                old_edge = None;
            }
            Some(edges_from_src) => {
                match edges_from_src.insert(edge.dest, edge.weight) {
                    None => old_edge = None,
                    Some(weight) => {
                        let mut edge = edge.clone();
                        edge.weight = weight;
                        old_edge = Some(edge);
                    }
                };
            }
        };
        old_edge
    }

    fn replace_all_edges_from_src(&mut self, edge: &Edge<NODE, WEIGHT>) {
        let mut edges_from_src = HashMap::new();
        edges_from_src.insert(edge.dest, edge.weight);
        self.edges.insert(edge.src, edges_from_src);
    }

    /// Removes an edge, returning it if it was previously in the graph.
    pub fn remove_edge(&mut self, edge: Edge<NODE, WEIGHT>) -> Option<Edge<NODE, WEIGHT>> {
        match self.edges.get_mut(&edge.src) {
            None => None,
            Some(edges_from_src) => match edges_from_src.remove(&edge.dest) {
                None => None,
                Some(weight) => Some(Edge {
                    src: edge.src,
                    dest: edge.dest,
                    weight,
                }),
            },
        }
    }

    /// Returns a vector conatining the adjacent nodes of the supplied node.
    ///
    /// Returns [`None`] if the node doesn't exist.
    pub fn adjacent_nodes(&self, node: &NODE) -> Option<Vec<NODE>> {
        match self.edges.get(node) {
            None => None,
            Some(edge) => Some(edge.keys().cloned().collect()),
        }
    }
}

macro_rules! scalar_op_impl {
    ($trait:ident $fn:ident) => {
        impl<NODE, WEIGHT> $trait<WEIGHT> for DirectedGraph<NODE, WEIGHT>
        where
            NODE: Debug + Display + Copy + Eq + Hash,
            WEIGHT: Num + Debug + Copy + PartialOrd + Hash,
        {
            type Output = DirectedGraph<NODE, WEIGHT>;
            fn $fn(self, rhs: WEIGHT) -> Self::Output {
                let mut edges = self.edges;
                for edge in edges.values_mut() {
                    for weight in edge.values_mut() {
                        *weight = weight.$fn(rhs);
                    }
                }
                Self { edges }
            }
        }
    };
}

scalar_op_impl!(Add add);
scalar_op_impl!(Sub sub);
scalar_op_impl!(Mul mul);
scalar_op_impl!(Div div);
scalar_op_impl!(Rem rem);
