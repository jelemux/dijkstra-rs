use super::directed::DirectedGraph;
use super::Edge;
use num_traits::Num;
use std::collections::HashMap;
use std::fmt::Debug;
use std::fmt::Display;
use std::hash::Hash;
use std::ops::Add;
use std::ops::Div;
use std::ops::Mul;
use std::ops::Sub;

/// Data structure that represents "connections" between pairs of elements.
///
/// Elements are called nodes.
/// The connections are called edges.
/// The connections between nodes go in both directions,
/// that's why this graph is undirected.
#[derive(Clone, Debug)]
pub struct UndirectedGraph<NODE, WEIGHT>
where
    NODE: Debug + Display + Copy + Eq + Hash,
    WEIGHT: Num + Debug + Copy + PartialOrd + Hash,
{
    directed_graph: DirectedGraph<NODE, WEIGHT>,
}

impl<NODE, WEIGHT> UndirectedGraph<NODE, WEIGHT>
where
    NODE: Debug + Display + Copy + Eq + Hash,
    WEIGHT: Num + Debug + Copy + PartialOrd + Hash,
{
    /// Creates an empty graph.
    pub fn new() -> Self {
        Self {
            directed_graph: DirectedGraph::new(),
        }
    }

    /// Returns a reference to the `edges` of the graph.
    pub fn edges(&self) -> &HashMap<NODE, HashMap<NODE, WEIGHT>> {
        &self.directed_graph.edges()
    }

    /// Returns all nodes in the graph.
    pub fn nodes(&self) -> Vec<NODE> {
        self.directed_graph.nodes()
    }

    /// Returns the weight of the edge between the nodes if the edge exists.
    pub fn weight(&self, src: &NODE, dest: &NODE) -> Option<WEIGHT> {
        self.directed_graph.weight(src, dest)
    }

    /// Inserts an edge into the graph.
    ///
    /// If the graph did not have the edge present, [`None`] is returned.
    ///
    /// If the graph did have this edge present, the value is updated, and the old
    /// value is returned.
    pub fn insert_edge(&mut self, edge: Edge<NODE, WEIGHT>) -> Option<Edge<NODE, WEIGHT>> {
        self.directed_graph.insert_edge(edge.reverse());
        self.directed_graph.insert_edge(edge)
    }

    /// Removes an edge, returning it if it was previously in the graph.
    pub fn remove_edge(&mut self, edge: Edge<NODE, WEIGHT>) -> Option<Edge<NODE, WEIGHT>> {
        self.directed_graph.remove_edge(edge.reverse());
        self.directed_graph.remove_edge(edge)
    }

    /// Returns a vector conatining the adjacent nodes of the supplied node.
    ///
    /// Returns [`None`] if the node doesn't exist.
    pub fn adjacent_nodes(&self, node: &NODE) -> Option<Vec<NODE>> {
        self.directed_graph.adjacent_nodes(node)
    }
}

macro_rules! scalar_op_impl {
    ($trait:ident $fn:ident) => {
        impl<NODE, WEIGHT> $trait<WEIGHT> for UndirectedGraph<NODE, WEIGHT>
        where
            NODE: Debug + Display + Copy + Eq + Hash,
            WEIGHT: Num + Debug + Copy + PartialOrd + Hash,
        {
            type Output = UndirectedGraph<NODE, WEIGHT>;
            fn $fn(self, rhs: WEIGHT) -> Self::Output {
                let directed_graph = self.directed_graph.$fn(rhs);
                Self { directed_graph }
            }
        }
    };
}

scalar_op_impl!(Add add);
scalar_op_impl!(Sub sub);
scalar_op_impl!(Mul mul);
scalar_op_impl!(Div div);
