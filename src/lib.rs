#![crate_name = "dijkstra_rs"]
//! # dijkstra-rs
//!
//! The [`Dijkstra algorithm`] implemented in Rust.
//!
//! I did this as an exercise for myself, but I tried to
//! document it well so maybe it will also help others.
//!
//! [`Dijkstra algorithm`]: https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm

/// This module contains the [dijkstra::Dijkstra] struct that is used
/// to calculate the shortest path between one source node and all other
/// nodes in a [graphs::undirected::UndirectedGraph].
pub mod dijkstra;

/// This module contains the [graphs::directed::DirectedGraph] and [graphs::undirected::UndirectedGraph] structs.
///
/// Also included is the [graphs::Edge] struct.
/// It is used to add new connection between nodes to the graph.
pub mod graphs;

#[cfg(test)]
mod tests {

    use crate::dijkstra::Dijkstra;
    use crate::graphs::undirected::UndirectedGraph;
    use crate::graphs::Edge;

    #[test]
    fn calculate_one() {
        let mut graph = UndirectedGraph::<usize, usize>::new();

        graph.insert_edge(Edge::<usize, usize> {
            src: 0,
            dest: 0,
            weight: 0,
        });

        let mut dijkstra = Dijkstra::<usize, usize>::init(graph, 0);
        dijkstra.calculate(None);
        let path = dijkstra.shortest_path(&0);

        assert_eq!(path, Some(vec![0]));
    }

    #[test]
    fn calculate_all() {
        let mut graph = UndirectedGraph::<usize, usize>::new();

        graph.insert_edge(Edge::<usize, usize> {
            src: 0,
            dest: 1,
            weight: 2,
        });
        graph.insert_edge(Edge::<usize, usize> {
            src: 0,
            dest: 2,
            weight: 6,
        });
        graph.insert_edge(Edge::<usize, usize> {
            src: 1,
            dest: 3,
            weight: 5,
        });
        graph.insert_edge(Edge::<usize, usize> {
            src: 2,
            dest: 3,
            weight: 8,
        });
        graph.insert_edge(Edge::<usize, usize> {
            src: 3,
            dest: 4,
            weight: 10,
        });
        graph.insert_edge(Edge::<usize, usize> {
            src: 3,
            dest: 5,
            weight: 15,
        });
        graph.insert_edge(Edge::<usize, usize> {
            src: 4,
            dest: 5,
            weight: 6,
        });
        graph.insert_edge(Edge::<usize, usize> {
            src: 4,
            dest: 6,
            weight: 2,
        });
        graph.insert_edge(Edge::<usize, usize> {
            src: 5,
            dest: 6,
            weight: 6,
        });

        let mut dijkstra = Dijkstra::<usize, usize>::init(graph, 0);
        dijkstra.calculate(None);
        let path = dijkstra.shortest_path(&6);

        assert_eq!(path, Some(vec![6, 4, 3, 1, 0]));
    }

    #[test]
    fn calculate_some() {
        let mut graph = UndirectedGraph::<&str, usize>::new();

        graph.insert_edge(Edge::<&str, usize> {
            src: "a",
            dest: "b",
            weight: 6,
        });
        graph.insert_edge(Edge::<&str, usize> {
            src: "b",
            dest: "c",
            weight: 5,
        });
        graph.insert_edge(Edge::<&str, usize> {
            src: "a",
            dest: "c",
            weight: 7,
        });
        graph.insert_edge(Edge::<&str, usize> {
            src: "a",
            dest: "e",
            weight: 2,
        });
        graph.insert_edge(Edge::<&str, usize> {
            src: "c",
            dest: "e",
            weight: 5,
        });
        graph.insert_edge(Edge::<&str, usize> {
            src: "e",
            dest: "d",
            weight: 4,
        });
        graph.insert_edge(Edge::<&str, usize> {
            src: "d",
            dest: "f",
            weight: 1,
        });
        graph.insert_edge(Edge::<&str, usize> {
            src: "e",
            dest: "f",
            weight: 2,
        });
        graph.insert_edge(Edge::<&str, usize> {
            src: "f",
            dest: "a",
            weight: 3,
        });

        let mut dijkstra = Dijkstra::<&str, usize>::init(graph, "a");
        dijkstra.calculate(Some("d"));

        let path_some = dijkstra.shortest_path(&"d");
        assert_eq!(path_some, Some(vec!["d", "f", "a"]));

        let path_none = dijkstra.shortest_path(&"b");
        assert_eq!(path_none, None);
    }
}
