use crate::graphs::undirected::UndirectedGraph;
use num_traits::Bounded;
use num_traits::Unsigned;
use std::collections::HashMap;
use std::collections::HashSet;
use std::fmt::Debug;
use std::fmt::Display;
use std::hash::Hash;

/// Data structure to store the states each visited or settled node can have
/// during computation of the Dijkstra algorithm.
#[derive(Clone, Debug)]
pub struct NodeState<NODE, WEIGHT>
where
    NODE: Debug + Display + Copy + Eq + Hash,
    WEIGHT: Debug + Unsigned + Bounded + Copy + PartialOrd + Hash,
{
    pub previous_node: Option<NODE>,
    pub distance: WEIGHT,
}

impl<NODE, WEIGHT> NodeState<NODE, WEIGHT>
where
    NODE: Debug + Display + Copy + Eq + Hash,
    WEIGHT: Debug + Unsigned + Bounded + Copy + PartialOrd + Hash,
{
    pub fn new(previous_node: Option<NODE>, distance: WEIGHT) -> Self {
        Self {
            previous_node,
            distance,
        }
    }
}

/// Data structure for executing the Dijkstra algorithm.
///
/// The Dijkstra algorithm calculates the shortest path
/// between the source node and all the nodes in the graph.
///
/// # Examples
/// ```
/// # use dijkstra_rs::graphs::undirected::UndirectedGraph;
/// # use dijkstra_rs::graphs::Edge;
/// # use dijkstra_rs::dijkstra::Dijkstra;
/// let mut graph = UndirectedGraph::<u32, u32>::new();
///
/// graph.insert_edge(Edge::<u32, u32>{
///     src: 0, dest: 1, weight: 2
/// });
/// graph.insert_edge(Edge::<u32, u32>{
///     src: 1, dest: 2, weight: 1
/// });
/// graph.insert_edge(Edge::<u32, u32>{
///     src: 2, dest: 3, weight: 2
/// });
/// graph.insert_edge(Edge::<u32, u32>{
///     src: 2, dest: 0, weight: 5
/// });
/// graph.insert_edge(Edge::<u32, u32>{
///     src: 1, dest: 3, weight: 4
/// });
///
/// let mut dijkstra = Dijkstra::<u32, u32>::init(graph, 0);
/// dijkstra.calculate(None);
/// let path = dijkstra.shortest_path(&3);
///
/// assert_eq!(path, Some(vec![3,2,1,0]));
/// ```
pub struct Dijkstra<NODE, WEIGHT>
where
    NODE: Debug + Display + Copy + Eq + Hash,
    WEIGHT: Debug + Unsigned + Bounded + Copy + PartialOrd + Hash,
{
    graph: UndirectedGraph<NODE, WEIGHT>,
    unvisited: HashSet<NODE>,
    visited: HashMap<NODE, NodeState<NODE, WEIGHT>>,
    settled: HashMap<NODE, NodeState<NODE, WEIGHT>>,
}

impl<NODE, WEIGHT> Dijkstra<NODE, WEIGHT>
where
    NODE: Debug + Display + Copy + Eq + Hash,
    WEIGHT: Debug + Unsigned + Bounded + Copy + PartialOrd + Hash,
{
    /// Initializes an instance with the specified source and graph and returns it.
    ///
    /// # Arguments
    /// * `graph` - The graph that contains all the connections between the different nodes. Must contain the source.
    /// * `source` - The source node to calculate the paths from.
    ///
    /// # Examples
    /// ```
    /// # use dijkstra_rs::graphs::undirected::UndirectedGraph;
    /// # use dijkstra_rs::graphs::Edge;
    /// # use dijkstra_rs::dijkstra::Dijkstra;
    /// let mut graph = UndirectedGraph::<usize, usize>::new();
    ///
    /// let dijkstra = Dijkstra::<usize, usize>::init(graph, 0);
    /// ```
    pub fn init(graph: UndirectedGraph<NODE, WEIGHT>, source: NODE) -> Self {
        let mut unvisited = HashSet::new();
        let mut visited = HashMap::new();
        visited.insert(source, NodeState::new(None, WEIGHT::zero()));
        let settled = HashMap::new();

        for node in graph.nodes() {
            if node != source {
                // insert the node with unlimited distance
                unvisited.insert(node);
            }
        }
        Self {
            graph,
            unvisited,
            visited,
            settled,
        }
    }

    /// Executes the Dijkstra algorithm to calculate the shortest path between
    /// the source node and all other nodes in the [`crate::graphs::undirected::UndirectedGraph`]
    /// until either the destination node is reached (if supplied) or all shortest paths are calculated.
    ///
    /// It then returns the instance of [`Dijkstra`].
    ///
    /// # Arguments
    /// * `destination` - Optional destination node to calculate the shortest path to.
    ///
    /// # Examples
    /// ```
    /// # use dijkstra_rs::graphs::undirected::UndirectedGraph;
    /// # use dijkstra_rs::graphs::Edge;
    /// # use dijkstra_rs::dijkstra::Dijkstra;
    /// let mut graph = UndirectedGraph::<usize, usize>::new();
    ///
    /// // shortest route is the first three edges
    /// graph.insert_edge(Edge::<usize, usize>{
    ///     src: 0, dest: 1, weight: 2
    /// });
    /// graph.insert_edge(Edge::<usize, usize>{
    ///     src: 1, dest: 2, weight: 1
    /// });
    /// graph.insert_edge(Edge::<usize, usize>{
    ///     src: 2, dest: 0, weight: 2
    /// });
    ///
    /// let mut dijkstra = Dijkstra::<usize, usize>::init(graph, 0);
    /// dijkstra.calculate(Some(2));
    /// ```
    pub fn calculate(&mut self, destination: Option<NODE>) {
        // We can just iterate over the Dijkstra struct, because we implemented the [`Iterator`] trait
        for node in self {
            if destination == Some(node) {
                break;
            }
        }
    }

    /// Return the nodes in the shortest path from the specified node to the source.
    pub fn shortest_path(&self, node: &NODE) -> Option<Vec<NODE>> {
        self.shortest_path_recursive(node, Vec::new())
    }

    /// Follows the shortest path recursively.
    fn shortest_path_recursive(&self, node: &NODE, mut path: Vec<NODE>) -> Option<Vec<NODE>> {
        let state = match self.settled.get(node) {
            None => return None,
            Some(state) => state,
        };

        path.push(*node);

        if let Some(prev) = state.previous_node {
            self.shortest_path_recursive(&prev, path)
        } else {
            Some(path)
        }
    }

    /// Returns all unvisited nodes.
    pub fn unvisited(&self) -> HashSet<NODE> {
        self.unvisited.clone()
    }

    /// Returns all visited not yet settled nodes and their states.
    pub fn visited(&self) -> HashMap<NODE, NodeState<NODE, WEIGHT>> {
        self.visited.clone()
    }

    /// Returns all settled nodes (the ones for which a shortest path has been found).
    pub fn settled(&self) -> HashMap<NODE, NodeState<NODE, WEIGHT>> {
        self.settled.clone()
    }

    /// Returns the visited but unsettled node with the minimal distance to the source node.
    ///
    /// If there are no more visited nodes return [`None`].
    fn visited_node_with_minimal_distance(&self) -> Option<NODE> {
        let mut visited_node_with_minimal_distance = (None, WEIGHT::max_value());

        for (node, state) in self.visited.iter() {
            if state.distance < visited_node_with_minimal_distance.1 {
                visited_node_with_minimal_distance = (Some(node), state.distance);
            }
        }

        match visited_node_with_minimal_distance.0 {
            None => None,
            Some(node) => Some(*node),
        }
    }

    /// Adds the nodes adjacent to the specified node to visited list with their respective distance and previous_node.
    fn relax_adjacent_nodes(&mut self, previous: &NODE) {
        let adjacent_nodes = self.graph.adjacent_nodes(previous)
            .expect("Should not happen, because the only way to add a one node graph is to let a node have an edge to itself and then `adjacent_nodes()` doesn't return `None`.");

        let previous_state = self
            .settled
            .get(previous)
            .expect("Should not happen as previous node should be settled.");
        let previous_distance = previous_state.distance;

        for node in adjacent_nodes {
            let weight = self.graph.weight(&node, previous).expect(&format!(
                "The edge between {} and {} should exist.",
                previous, node
            ));

            let new_distance = previous_distance + weight;
            self.visit_node(&node, NodeState::new(Some(*previous), new_distance));
        }
    }

    /// Visits the specified node.
    ///
    /// If the node was visited before, its state is updated with the new state.
    ///
    /// If the node was unvisited, it is set to visited with the new state as its state.
    fn visit_node(&mut self, node: &NODE, new_state: NodeState<NODE, WEIGHT>) {
        match self.visited.get_mut(node) {
            None => match self.unvisited.get(node) {
                None => (),
                Some(_) => {
                    self.visited.insert(*node, new_state);
                    self.unvisited.remove(node);
                }
            },
            Some(state) => {
                if state.distance > new_state.distance {
                    *state = new_state;
                }
            }
        }
    }

    /// Settles the node if it was visited and returns it.
    ///
    /// Otherwise returns [`None`].
    fn settle_node(&mut self, node: &NODE) -> Option<NODE> {
        match self.visited.get_mut(node) {
            None => None,
            Some(state) => {
                self.settled.insert(*node, state.clone());
                self.visited.remove(node);
                Some(*node)
            }
        }
    }
}

/// Implement the [`Iterator`] trait which gives us the ability
/// to iterate over our [`Dijkstra`] struct and much more.
impl<NODE, WEIGHT> Iterator for Dijkstra<NODE, WEIGHT>
where
    NODE: Debug + Display + Copy + Eq + Hash,
    WEIGHT: Debug + Unsigned + Bounded + Copy + PartialOrd + Hash,
{
    type Item = NODE;
    /// Calculates the next visited node with minimal distance to the source,
    /// settles it, relaxes its neighbors and returns it.
    ///
    /// Returns [`None`] if there is no next node.
    /// This usually means that we've reached the source node.
    fn next(&mut self) -> Option<Self::Item> {
        let next_node = self.visited_node_with_minimal_distance();

        match next_node {
            None => return None,
            Some(next_node) => {
                self.settle_node(&next_node);
                self.relax_adjacent_nodes(&next_node);
            }
        };

        next_node
    }
}
