use num_traits::Num;
use std::fmt::Debug;
use std::fmt::Display;
use std::hash::Hash;

pub mod directed;
pub mod undirected;

/// Edge between two nodes.
///
/// The weight could for example represent distance or possible speed between the two nodes.
#[derive(Clone, Debug)]
pub struct Edge<NODE, WEIGHT>
where
    NODE: Debug + Display + Copy + Eq + Hash,
    WEIGHT: Num + Debug + Copy + PartialOrd + Hash,
{
    pub src: NODE,
    pub dest: NODE,
    pub weight: WEIGHT,
}

impl<NODE, WEIGHT> Edge<NODE, WEIGHT>
where
    NODE: Debug + Display + Copy + Eq + Hash,
    WEIGHT: Num + Debug + Copy + PartialOrd + Hash,
{
    pub fn reverse(&self) -> Self {
        Self {
            src: self.dest,
            dest: self.src,
            weight: self.weight,
        }
    }
}
