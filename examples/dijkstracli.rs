use clap::{App, Arg, ArgMatches};
use dijkstra_rs::dijkstra::Dijkstra;
use dijkstra_rs::graphs::undirected::UndirectedGraph;
use dijkstra_rs::graphs::Edge;
use std::process;

pub fn main() {
    let matches = arg_matches();

    let graph = match matches.values_of("edge") {
        None => process::exit(1),
        Some(edges_raw) => build_graph_from_arg_values(edges_raw),
    };

    let source_node = match matches.value_of("source") {
        None => process::exit(1),
        Some(src_node) => src_node,
    };

    let destination_nodes = match matches.values_of("destination") {
        None => process::exit(1),
        Some(dest_nodes) => dest_nodes,
    };

    let mut dijkstra = Dijkstra::init(graph, source_node);

    for dest_node in destination_nodes {
        dijkstra.calculate(Some(dest_node));
        let shortest_path = dijkstra.shortest_path(&dest_node).unwrap();
        print_path(shortest_path);
    }

    process::exit(0);
}

fn print_path(path: Vec<&str>) {
    println!(
        "The shortest path from source {} to destination {} is:",
        path[path.len() - 1],
        path[0]
    );
    for node in path.iter().rev() {
        print!("{}", node);
        if node != &path[0] {
            print!(" -> ");
        }
    }
    println!();
    println!();
}

fn build_graph_from_arg_values(arg_values: clap::Values) -> UndirectedGraph<&str, usize> {
    let mut graph = UndirectedGraph::new();
    for edge_raw in arg_values {
        let mut edge_raw_split = edge_raw.split(",");

        let src = edge_raw_split.next().unwrap();
        let dest = edge_raw_split.next().unwrap();

        let weight = edge_raw_split.next().unwrap().parse::<usize>().unwrap();

        graph.insert_edge(Edge { src, dest, weight });
    }

    graph
}

fn arg_matches() -> ArgMatches<'static> {
    App::new("DijkstraCLI")
        .version(env!("CARGO_PKG_VERSION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .about("An Example for a commandline program to calculate the shortest path between two points using the Dijkstra Algorithm")
        .arg(Arg::with_name("source")
                .short("s")
                .long("source")
                .takes_value(true)
                .required(true)
                .empty_values(false)
                .display_order(1)
                .help("The specifier of the source node in the graph."))
        .arg(Arg::with_name("destination")
                .short("d")
                .long("dest")
                .takes_value(true)
                .required(true)
                .empty_values(false)
                .multiple(true)
                .display_order(2)
                .help("The specifier of the destination node in the graph. Can be used more than once."))
        .arg(Arg::with_name("edge")
                .short("e")
                .long("edge")
                .takes_value(true)
                .required(true)
                .empty_values(false)
                .multiple(true)
                .validator(validate_edge)
                .value_name("node,node,weight")
                .display_order(3)
                .help("An edge in the graph. An edge consists of two nodes and one weight, seperated by commas."))
        .get_matches()
}

fn validate_edge(edge: String) -> Result<(), String> {
    let mut values = edge.split(",");

    if let None = values.nth(1) {
        return Err(String::from("The edge must contain two nodes"));
    }

    match values.next() {
        Some(weight) => {
            if weight.chars().all(char::is_numeric) {
                Ok(())
            } else {
                Err(String::from("The weight must be a positive integer"))
            }
        }
        None => Err(String::from("The edge must contain a weight")),
    }
}
