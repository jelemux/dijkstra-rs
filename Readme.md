# Dijkstra Algorithm in Rust

As an exercise for myself, I implemented the [Dijkstra algorithm](https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm) in Rust.

## 🚲 Usage

### 🛠️ Needed tools

[Standard rust toolchain](https://www.rust-lang.org/tools/install)


### 📦 Build
Debug:
```
cargo build --debug
```
Release:
```
cargo build --release
```
### Example dijkstracli
Build:
```
cargo build --example dijkstracli
```
The binary should then be located under target/debug/examples

Run:
```
cargo run --example dijkstracli -- -s 0 -d 6 -e 0,1,2 0,2,6 1,3,5 2,3,8 3,4,10 3,5,15 4,5,6 4,6,2 5,6,6
```
### 🗒 Test
```
cargo test
```